function JsonData5(value) {
  const data5 = value.filter(
    (data) => data.registered <= "2016-01-01" && data.isActive === true
  );
  return data5;
}

const data = require('./data')
module.exports = JsonData5(data);