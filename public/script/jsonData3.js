function JsonData3(value) {
  const data3 = value.filter(
    (data) => data.eyeColor === "blue" &&
    data.age >= 35 &&
    data.age <= 40 &&
    data.favoriteFruit === "apple"
  );
  return data3;
}

const data = require('./data')
module.exports = JsonData3(data);