function JsonData1(value) {
  const data1 = value.filter(
    (data) => data.age < 30 && data.favoriteFruit === "banana"
  );
  return data1;
}

const data = require('./data')
module.exports = JsonData1(data);